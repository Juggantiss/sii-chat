import React from 'react';
import Navigator from './src';
import ApolloClient from './apolloSetup';
import { ApolloProvider } from '@apollo/client';

export default function App() {
  return (
    <ApolloProvider client={ApolloClient}>
      <Navigator />
    </ApolloProvider>
  );
}
