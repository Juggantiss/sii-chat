## Configuración para el uso de variables de entorno:

- Instala las dependencias nuevas

  ```bash
  npm i
  ```

- Borra el caché de babel

  ```bash
  expo r -c
  ```

- Las próximas veces se puede correr normal con `expo start` o `npm start`
