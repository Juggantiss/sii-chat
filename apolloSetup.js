import {
  ApolloClient,
  HttpLink,
  InMemoryCache,
  split,
} from 'apollo-client-preset';
import { setContext } from 'apollo-link-context';
import { getToken } from './token';
import { API_URL, WS_API_URL } from '@env';

// Subscription dependencies
import { WebSocketLink } from '@apollo/client/link/ws';
import { getMainDefinition } from '@apollo/client/utilities';

// Subscriptions config
const wsLink = new WebSocketLink({
  uri: WS_API_URL,
  options: {
    reconnect: true,
    connectionParams: async () => {
      const token = await getToken();
      return {
        authorization: token ? `Bearer ${token}` : '',
      };
    },
  },
});

const httpLink = new HttpLink({
  uri: API_URL,
});

const authLink = setContext(async (req, { headers }) => {
  const token = await getToken();
  return {
    ...headers,
    headers: {
      authorization: token ? token : '',
    },
  };
});

const link = split(
  // split based on operation type
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query);
    return kind === 'OperationDefinition' && operation === 'subscription';
  },
  wsLink,
  authLink.concat(httpLink)
);

export default new ApolloClient({
  link,
  cache: new InMemoryCache(),
});
