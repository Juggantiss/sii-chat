import React from 'react';
import { TouchableOpacity, StyleSheet, View, Button, Text } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';

export default function BtnConfig() {
  const navigation = useNavigation();
  const goToConfig = () => {
    navigation.navigate('Setting');
  };

  return (
    <>
      <TouchableOpacity style={styles.contBtn} onPress={goToConfig}>
        <Ionicons name='ellipsis-vertical' size={20} color='#fff' />
      </TouchableOpacity>
    </>
  );
}

const styles = StyleSheet.create({
  contBtn: {
    backgroundColor: '#013A7E',
    width: 50,
    height: 50,
    marginHorizontal: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
