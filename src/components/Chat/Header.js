import React from "react";
import { View, StyleSheet, TouchableOpacity, Image, Text } from "react-native";

import { Ionicons } from "@expo/vector-icons";

const Header = (props) => {
  const { navigation, name, uri } = props;

  return (
    <View style={styles.header}>
      <View style={styles.contBtn}>
        <TouchableOpacity
          style={styles.btnIcon}
          onPress={() => navigation.navigate("Home")}
        >
          <Ionicons name="arrow-back" size={25} color="#fff" />
        </TouchableOpacity>
      </View>
      <View style={styles.contProfile}>
        <TouchableOpacity style={styles.btnProfile}>
          <Image style={styles.imgUser} source={{ uri }} />
          <Text style={styles.txtName}>{name}</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.contBtn}>
        <TouchableOpacity style={styles.btnIcon}>
          <Ionicons
            style={styles.icon}
            name="ellipsis-vertical"
            size={20}
            color="#fff"
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    width: "100%",
    height: 55,
    backgroundColor: "#03004C",
    flexDirection: "row",
    position: "absolute",
    top: 0,
    zIndex: 1,
  },
  contBtn: {
    width: "15%",
    height: "100%",
    // backgroundColor: "#f00",
    justifyContent: "center",
    alignItems: "center",
  },
  btnIcon: {
    width: 40,
    height: 40,
    // backgroundColor: "#ff0",
    borderRadius: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  contProfile: {
    width: "70%",
    height: "100%",
    // backgroundColor: "#ff0",
  },
  btnProfile: {
    width: "100%",
    height: "100%",
    // backgroundColor: "#f00",
    alignItems: "center",
    flexDirection: "row",
  },
  imgUser: {
    width: 40,
    height: 40,
    borderRadius: 20,
    marginRight: 10,
  },
  txtName: {
    color: "#fff",
    fontSize: 20,
  },
});

export default Header;
