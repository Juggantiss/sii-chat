import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

const MsgReceiver = ({ text, hour }) => {
  return (
    <View style={styles.contReceptor}>
      <Text style={styles.txtReceptor}>{text}</Text>
      <View style={styles.contData}>
        <Text style={[styles.txtHora, { color: '#fff' }]}>{hour}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  contReceptor: {
    marginLeft: 30,
    marginVertical: 8,
    width: '80%',
    backgroundColor: '#013A7E',
    borderTopRightRadius: 15,
    borderBottomRightRadius: 15,
    borderBottomLeftRadius: 15,
  },
  txtReceptor: {
    color: '#fff',
    paddingTop: 10,
    paddingHorizontal: 10,
  },
  contData: {
    // backgroundColor: "#f00",
    alignItems: 'flex-end',
    paddingRight: 15,
    paddingBottom: 5,
  },
  txtHora: {
    fontSize: 11,
  },
});

export default MsgReceiver;
