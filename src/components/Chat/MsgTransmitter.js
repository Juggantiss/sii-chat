import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

const MsgTransmitter = ({ text, hour }) => {
  return (
    <View style={styles.contMensaje}>
      <View style={styles.contEmisor}>
        <Text style={styles.txtEmisor}>{text}</Text>
        <View style={styles.contData}>
          <Text style={[styles.txtHora, { color: '#03004C' }]}>{hour}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  contMensaje: {
    // backgroundColor: "#ff0",
    alignItems: 'flex-end',
  },
  contEmisor: {
    marginRight: 30,
    marginVertical: 8,
    width: '80%',
    backgroundColor: '#F3F4F4',
    borderTopLeftRadius: 15,
    borderBottomRightRadius: 15,
    borderBottomLeftRadius: 15,
  },
  txtEmisor: {
    color: '#03004C',
    paddingTop: 10,
    paddingHorizontal: 10,
  },
  contData: {
    // backgroundColor: "#f00",
    alignItems: 'flex-end',
    paddingRight: 15,
    paddingBottom: 5,
  },
  txtHora: {
    fontSize: 11,
  },
});

export default MsgTransmitter;
