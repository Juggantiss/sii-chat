import React from 'react';
import { View, StyleSheet, TouchableOpacity, TextInput } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

const SendMsg = (props) => {
  const { value, onChangeText, onPress, placeholder, sending } = props;

  return (
    <View style={styles.contSend}>
      <View style={styles.contInput}>
        <TextInput
          style={styles.input}
          placeholder={placeholder || 'Escribir mensaje'}
          value={value}
          onChangeText={onChangeText}
          placeholderTextColor='#BDC3C7'
          color='#03004C'
        />
      </View>
      <View style={styles.contBtnsend}>
        <TouchableOpacity style={styles.btnSend} onPress={onPress}>
          <Ionicons
            name='send'
            size={20}
            color={sending ? '#BDC3C7' : '#fff'}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  contSend: {
    width: '100%',
    // height: "15%",
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 12,
    position: 'absolute',
    bottom: 0,
    zIndex: 1,
  },
  contInput: {
    width: '80%',
    marginRight: 5,
    // height: 55,
    // backgroundColor: "#ff0",
  },
  input: {
    width: '100%',
    height: 50,
    backgroundColor: '#F3F4F4',
    borderRadius: 50,
    paddingHorizontal: 18,
  },
  contBtnsend: {
    width: '20%',
    height: 55,
    // backgroundColor: "#f00",
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnSend: {
    backgroundColor: '#03004C',
    width: 50,
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default SendMsg;
