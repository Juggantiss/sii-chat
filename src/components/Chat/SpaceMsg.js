import React, { useRef } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import MsgReceiver from './MsgReceiver';
import MsgTransmitter from './MsgTransmitter';

import ItiCrypt from '../../utils/ItiCrypt';

const SpaceMsg = ({ messages, id, recent }) => {
  // const [messages, setMessages] = useState(data);
  // if (recent) {
  //   Object.keys(recent).length !== 0 && setMessages([...messages, recent]);
  // }
  const itiCrypt = new ItiCrypt();
  const scrollViewRef = useRef();

  return (
    <View style={styles.contMsg}>
      <View style={styles.spaceMsg}>
        <ScrollView
          style={styles.scrollView}
          ref={scrollViewRef}
          onContentSizeChange={() =>
            scrollViewRef.current.scrollToEnd()
          }
        >
          {messages.map((message) => {
            const messageInfo = {
              text: itiCrypt.readMessage(message.message),
              hour: message.hour.slice(0, message.hour.length - 8),
            };
            return message.from.id === id ? (
              <MsgTransmitter key={message.id} {...messageInfo} />
            ) : (
              <MsgReceiver key={message.id} {...messageInfo} />
            );
          })}
        </ScrollView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  contMsg: {
    width: '100%',
    height: '100%',
    backgroundColor: '#03004C',
    paddingBottom: 79,
    // borderTopRightRadius: 35,
    // borderTopLeftRadius: 35,
    // paddingHorizontal: 30,
  },
  spaceMsg: {
    width: '100%',
    height: '100%',
    backgroundColor: '#fff',
    borderTopRightRadius: 35,
    borderTopLeftRadius: 35,
    // paddingHorizontal: 30,
  },
});

export default SpaceMsg;
