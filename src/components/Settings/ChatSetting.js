import React, { useState } from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  View,
  Text,
  ScrollView,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { RadioButton } from 'react-native-paper';

export default function ChatSetting() {
  const [checked, setChecked] = useState('uno');

  const Item = ({ icon, txtName }) => (
    <>
      <TouchableOpacity style={styles.item}>
        <View style={styles.itemIcon}>
          <Ionicons name={icon} size={25} color='#03004C' />
        </View>
        <View style={styles.itemInfo}>
          <Text style={styles.itemName}>{txtName}</Text>
        </View>
      </TouchableOpacity>
    </>
  );
  return (
    <>
      <ScrollView style={styles.scrollView}>
        <Text style={[styles.title, { marginBottom: 20 }]}>
          Protege tus conversaciones.
        </Text>
        <TouchableOpacity style={styles.item} onPress={() => setChecked('uno')}>
          <RadioButton
            style={styles.radio}
            value='uno'
            status={checked === 'uno' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('uno')}
          />
          <Text style={styles.itemName}>Conservar mensajes</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.item} onPress={() => setChecked('dos')}>
          <RadioButton
            value='dos'
            status={checked === 'dos' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('dos')}
          />
          <Text style={styles.itemName}>Borrar mensajes solo con</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.item}
          onPress={() => setChecked('tres')}
        >
          <RadioButton
            value='tres'
            status={checked === 'tres' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('tres')}
          />
          <Text style={styles.itemName}>Borrar solo para mi</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.item}
          onPress={() => setChecked('cuatro')}
        >
          <RadioButton
            value='cuatro'
            status={checked === 'cuatro' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('cuatro')}
          />
          <Text style={styles.itemName}>Borrar para todos</Text>
        </TouchableOpacity>
        <Text style={[styles.desc, { marginTop: 20 }]}>
          Tus mensajes son solo para ti y las personas con las que elijas
          compartirlos, ni siquiera Biseenda prodra leerlos
        </Text>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 20,
  },
  title: {
    color: '#03004C',
    fontSize: 18,
  },
  desc: {
    color: '#959595',
    fontSize: 15,
  },
  item: {
    width: '100%',
    height: 40,
    marginTop: 10,
    // backgroundColor: '#ff0',
    alignItems: 'center',
    flexDirection: 'row',
  },
  itemName: {
    color: '#03004C',
    fontSize: 14,
  },
  radio: {
    color: '#03004C',
    backgroundColor: '#ff0',
    fontSize: 14,
  },
});
