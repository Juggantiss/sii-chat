import React from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  View,
  Text,
  ScrollView,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';

export default function Cuenta() {

  const navigation = useNavigation();

  const Item = ({ nav, icon, txtName }) => (
    <>
      <TouchableOpacity
        style={styles.item}
        onPress={() => navigation.navigate(nav)}
      >
        <View style={styles.itemIcon}>
          <Ionicons name={icon} size={25} color='#03004C' />
        </View>
        <View style={styles.itemInfo}>
          <Text style={styles.itemName}>{txtName}</Text>
        </View>
      </TouchableOpacity>
    </>
  );
  return (
    <>
      <ScrollView style={styles.scrollView}>
        <Item nav='ChatSetting' icon='ios-chatbox-ellipses' txtName='Chat' />
        <Item
          nav='ChatSetting'
          icon='ios-phone-portrait'
          txtName='Cambiar número'
        />
        <Item nav='ChatSetting' icon='trash' txtName='Eliminar cuenta' />
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    backgroundColor: '#fff',
    paddingVertical: 20,
  },
  item: {
    width: '100%',
    height: 50,
    marginBottom: 10,
    // backgroundColor: '#ff0',
    flexDirection: 'row',
  },
  itemIcon: {
    // backgroundColor: '#00f',
    justifyContent: 'center',
    alignItems: 'center',
    width: '18%',
    height: '100%',
  },
  itemInfo: {
    // backgroundColor: '#f00',
    justifyContent: 'center',
    width: '82%',
    height: '100%',
  },
  itemName: {
    color: '#03004C',
    fontSize: 15,
  },
  itemDesc: {
    color: '#959595',
    fontSize: 12,
  },
  contInfo: {
    // backgroundColor: '#f00',
    width: '100%',
    height: 200,
    paddingHorizontal: 20,
    alignItems: 'center',
  },
});
