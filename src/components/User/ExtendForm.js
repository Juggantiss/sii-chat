import React from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';

export default function ExtendForm(params) {
  const { iconName, fieldName, paragraph, ...rest } = params;
  return (
    <View style={styles.contInfo}>
      <View style={styles.contIcon}>
        <FontAwesome5 style={styles.icon} name={iconName} color='#000' />
      </View>
      <View style={styles.contInput}>
        <Text style={styles.infoName}>{fieldName}</Text>
        <TextInput {...rest} />
        {/* <Text style={styles.parrafo}>{paragraph}</Text> */}
      </View>
    </View>
  );
}

// const { height } = (Dimensions.get('window')*.2) + 25;
const styles = StyleSheet.create({
  icon: {
    color: '#fff',
    fontSize: 20,
  },
  contInfo: {
    // backgroundColor: "#f00",
    width: '80%',
    flexDirection: 'row',
    // height: 150,
  },
  contIcon: {
    // backgroundColor: "#f0f",
    width: '20%',
    height: 80,
    alignItems: 'center',
  },
  contInput: {
    // backgroundColor: "#00f",
    width: '80%',
    height: 80,
  },
  infoName: {
    color: '#BDC3C7',
    fontSize: 12,
  },
  input: {
    // backgroundColor: "#000",
    width: '100%',
    height: 40,
    fontSize: 10,
  },
  parrafo: {
    color: '#BDC3C7',
    fontSize: 12,
  },
});
