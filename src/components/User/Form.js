import React from "react";
import { FontAwesome5 } from "@expo/vector-icons";
import BottomSheet from "reanimated-bottom-sheet";

import {
  View,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Text,
} from "react-native";

const Form = (props) => {
  const { value, onChangeText, onPressNext } = props;

  const sheetRef = React.useRef(null);

  const renderContent = () => (
    <View
      style={{
        backgroundColor: "#f00",
        // padding: 16,
        height: 400,
      }}
    >
      <Text>Swipe down to close</Text>
    </View>
  );

  return (
    <>
      <View style={styles.contInput}>
        <FontAwesome5 style={styles.iconInput} name="phone" />
        <TextInput
          keyboardType="number-pad"
          maxLength={10}
          style={{ width: "90%" }}
          value={value}
          onChangeText={onChangeText}
          placeholder="Ingresa tu número de teléfono"
          placeholderTextColor="#BDC3C7"
          color="#03004C"
        />
      </View>
      <TouchableOpacity
        style={[styles.btnPrimary, styles.btn]}
        onPress={onPressNext}
      >
        <Text style={styles.textBtn}>Siguiente</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={[styles.btnSecondary, styles.btnPrivacity]}
        onPress={() => sheetRef.current.snapTo(0)}
      >
        <Text style={styles.textBtnPrivate}>Politicas de privacidad</Text>
      </TouchableOpacity>
      {/* <BottomSheet
          ref={sheetRef}
          snapPoints={[400, "0%", 0]}
          initialSnap={0}
          enabledBottomInitialAnimation={false}
          borderRadius={10}
          renderContent={renderContent}
        /> */}
    </>
  );
};

const styles = StyleSheet.create({
  contInput: {
    width: "80%",
    height: 50,
    // backgroundColor: "#f00",
    borderColor: "#BDC3C7",
    borderBottomWidth: 1,
    marginBottom: "5%",
    alignItems: "center",
    flexDirection: "row",
  },
  iconInput: {
    color: "#03004C",
    fontSize: 17,
    marginRight: 10,
  },
  btn: {
    width: "50%",
    height: 45,
    borderRadius: 25,
    justifyContent: "center",
    alignItems: "center",
    marginTop: "10%",
  },
  btnPrimary: {
    backgroundColor: "#03004C",
  },
  btnSecondary: {
    backgroundColor: "#fff",
    marginTop: "20%",
  },
  textBtn: {
    color: "#fff",
  },
  textBtnPrivate: {
    color: "#BDC3C7",
  },
});

export default Form;
