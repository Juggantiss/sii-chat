import React, { useRef } from 'react';
import {
  StatusBar,
  TouchableOpacity,
  StyleSheet,
  View,
  Button,
  Text,
} from 'react-native';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Ionicons } from '@expo/vector-icons';

import Home from './views/Home';
import Contacts from './views/Contacts';
import User from './views/Register/User';
import NewUser from './views/Register/NewUser';
import Chat from './views/Chat';
import Splash from './views/Splash';
import Login from './views/Login';
import Setting from './views/Setting';
import Cuenta from './components/Settings/Cuenta';
import ChatSetting from './components/Settings/ChatSetting';

//Buttons Header
import BtnConfigb from './components/Button/BtnConfig';
import BottomSheet from 'reanimated-bottom-sheet';

const Stack = createStackNavigator();

const BtnHeader = ({ icon }) => {
  return (
    <>
      <TouchableOpacity style={styles.contBtn}>
        <Ionicons name={icon} size={20} color='#fff' />
      </TouchableOpacity>
    </>
  );
};
const BtnConfig = () => (
  <>
    <TouchableOpacity style={styles.contBtn}>
      <Ionicons name='ellipsis-vertical' size={20} color='#fff' />
    </TouchableOpacity>
  </>
);

export default () => (
  <NavigationContainer>
    <StatusBar backgroundColor='#03004C' barStyle='light-content' />
    <Stack.Navigator initialRouteName='Splash'>
      <Stack.Screen
        name='Home'
        component={Home}
        options={{
          title: 'Chat',
          headerTitleStyle: { alignSelf: 'center' },
          headerStyle: {
            backgroundColor: '#03004C',
            elevation: 0,
          },
          headerTintColor: '#fff',
          headerLeft: () => <BtnConfigb />,
          headerRight: () => <BtnHeader icon={'search'} />,
        }}
      />
      <Stack.Screen
        name='Contacts'
        component={Contacts}
        options={{
          title: 'Contactos',
          // headerTitleStyle: { alignSelf: "center" },
          headerStyle: {
            backgroundColor: '#03004C',
            elevation: 0,
          },
          headerTintColor: '#fff',
          headerRight: () => <BtnHeader icon={'search'} />,
        }}
      />
      <Stack.Screen
        name='User'
        component={User}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name='Chat'
        component={Chat}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name='NewUser'
        component={NewUser}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name='Splash'
        component={Splash}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name='Login'
        component={Login}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name='Setting'
        component={Setting}
        options={{
          title: 'Ajustes',
          headerStyle: {
            backgroundColor: '#03004C',
            elevation: 0,
          },
          headerTintColor: '#fff',
        }}
      />
      <Stack.Screen
        name='Cuenta'
        component={Cuenta}
        options={{
          title: 'Cuenta',
          headerStyle: {
            backgroundColor: '#03004C',
            elevation: 0,
          },
          headerTintColor: '#fff',
        }}
      />
      <Stack.Screen
        name='ChatSetting'
        component={ChatSetting}
        options={{
          title: 'Ajustes de chat',
          headerStyle: {
            backgroundColor: '#03004C',
            elevation: 0,
          },
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  </NavigationContainer>
);

const styles = StyleSheet.create({
  contBtn: {
    // backgroundColor: "#f00",
    width: 50,
    height: 50,
    marginHorizontal: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
