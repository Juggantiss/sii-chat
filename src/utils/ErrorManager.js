import { Alert } from 'react-native';

class ErrorManager {
  error = null;
  constructor(error) {
    const stringifyErrors = JSON.stringify(error);
    this.error = JSON.parse(stringifyErrors);
  }

  getMessage() {
    if (typeof error === 'string') return error;
    const message = this.error?.message;
    if (!message) return 'Estamos trabajando en esto, una disculpa';
    const pureMessage = message.split(': ');
    return pureMessage[pureMessage.length - 1];
  }

  showMessage(title) {
    const message = this.getMessage();
    Alert.alert(title ? title : 'Verifique su información', message);
  }
}

export default ErrorManager;
