import { SECRET_KEY, KEY_ROUNDS, KEY_SIZE } from '@env';
class ItiCrypt {
  text = '';
  constructor(text = '') {
    this.text = text;
  }

  genKey() {
    let count = 0;
    let key = '';
    while (count !== parseInt(KEY_SIZE)) {
      key += String.fromCodePoint(
        Math.floor(Math.random() * parseInt(KEY_ROUNDS))
      );
      count += 1;
    }
    return key;
  }

  sign(text = '', key = '') {
    try {
      let count = 0;
      let signedText = '';
      for (let letter of text) {
        if (count === key.length) {
          count = 0;
        }
        const code = parseInt(letter.codePointAt(0));
        const newKey = parseInt(key.charAt(count).codePointAt(0));
        const suma_de_codigos = code + newKey;
        signedText += String.fromCodePoint(suma_de_codigos);
        count += 1;
      }
      return signedText;
    } catch (error) {
      console.log(error);
      return 'not possible';
    }
  }

  read(data = '', key = '') {
    try {
      let original = '';
      let count = 0;
      for (let letter of data) {
        if (count === key.length) {
          count = 0;
        }
        const letterCode = parseInt(letter.codePointAt(0));
        const extractedKey = parseInt(key.charAt(count).codePointAt(0));
        const originalLetter = letterCode - extractedKey;
        if (originalLetter === 32) {
          original += ' ';
        } else {
          original += String.fromCodePoint(originalLetter);
        }
        count += 1;
      }
      return original;
    } catch (error) {
      throw new Error(error);
    }
  }

  hashMessage() {
    const key = this.genKey();
    const textHashed = this.sign(this.text, key);
    const basicFormat = `${textHashed}.${key}`;
    const textHashedHashed = this.sign(basicFormat, SECRET_KEY);
    return textHashedHashed;
  }

  readMessage(data) {
    try {
      const basicFormat = this.read(data, SECRET_KEY);
      const elements = basicFormat.split('.');
      const normalText = this.read(elements[0], elements[1]);
      return normalText;
    } catch (error) {
      return data;
    }
  }
}

export default ItiCrypt;
