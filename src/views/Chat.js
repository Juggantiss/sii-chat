import React, { useState, useEffect } from 'react';
import ItiCrypt from '../utils/ItiCrypt';
import { getToken } from '../../token';

import ErrorManager from '../utils/ErrorManager';

import Header from '../components/Chat/Header';
import SpaceMsg from '../components/Chat/SpaceMsg';
import SendMsg from '../components/Chat/SendMsg';

import { gql, useQuery, useMutation, useSubscription } from '@apollo/client';

import { StyleSheet, View } from 'react-native';
import Loading from '../components/Loading';

const ME = gql`
  query {
    me {
      id
    }
  }
`;

const MESSAGES_QUERY = gql`
  query getMessages($contactId: ID!) {
    getMessages(contactId: $contactId) {
      id
      message
      hour
      date
      state
      from {
        id
      }
    }
  }
`;

const SEND_MESSAGE_MUTATION = gql`
  mutation ($input: SendMessageInput!) {
    sendMessage(input: $input)
  }
`;

const WAITING_FOR_MESSAGES = gql`
  subscription {
    newMessage {
      id
      message
      hour
      date
      state
      from {
        id
      }
    }
  }
`;

export default function Chat({ route, navigation }) {
  const [messageList, setMessageList] = useState([]);
  const [textMessage, setTextMessage] = useState('');
  const [sending, setSending] = useState(false);

  // GRAPHQL REQUEST's
  const my_data = useQuery(ME);
  const messages = useQuery(MESSAGES_QUERY, {
    variables: { contactId: route.params.id },
    fetchPolicy: 'no-cache',
  });
  const [sendMessage] = useMutation(SEND_MESSAGE_MUTATION);
  const waitingForMessages = useSubscription(WAITING_FOR_MESSAGES);

  useEffect(() => {
    if (messages.data) {
      setMessageList(messages.data.getMessages);
    }
  }, [messages]);

  useEffect(() => {
    if (waitingForMessages.data) {
      setMessageList([...messageList, waitingForMessages.data.newMessage]);
    }
  }, [waitingForMessages.data]);

  const handleSendMessage = async () => {
    if (textMessage.trim().length === 0 || sending) return;
    try {
      setSending(true);
      const itiCrypt = new ItiCrypt(textMessage);
      const { data } = await sendMessage({
        variables: {
          input: {
            message: itiCrypt.hashMessage(),
            to: route.params.id,
          },
        },
      });
      if (data.sendMessage.estado) {
        setTextMessage('');
        return;
      }
      throw { message: 'Esto no debio pasar, una disculpa' };
    } catch (error) {
      const errorManager = new ErrorManager(error);
      errorManager.showMessage('Error (Solucionar)');
    } finally {
      setSending(false);
    }
  };

  if (my_data.loading || messages.loading) {
    return <Loading />;
  }

  return (
    <>
      <Header
        navigation={navigation}
        name={route.params?.name || 'Usuario'}
        uri={route.params?.uri}
      />
      {my_data.data && messageList.length > 0 && (
        <View style={styles.container}>
          <SpaceMsg messages={messageList} id={my_data.data.me.id} />
        </View>
      )}
      <SendMsg
        value={textMessage}
        onChangeText={(text) => setTextMessage(text)}
        onPress={handleSendMessage}
        sending={sending}
      />
    </>
  );

  return null;
}

/**
 * 
  if (!loadingMessage) {
    // if (message !== null && message.nuevoMensaje !== null) {
    //   const id = message.nuevoMensaje.id;
    //   const time = message.nuevoMensaje.time;
    //   const from = message.nuevoMensaje.from.id;
    //   const text = message.nuevoMensaje.message;
    //   recentMessage = { id, time, from, text };
    // }
    refetch();
  }

  if (error) {
    console.log(error);
    alert('Ha ocurrido un error al traer los mensajes');
  }

  if (errorMessage) {
    console.log(errorMessage);
    alert('Ha ocurrido un error al traer los mensajes');
  }

  const onPress = async () => {
    if (messageSend !== '') {
      const { data } = await newMessage({
        variables: {
          input: {
            message: messageSend,
            from: idUser,
            to: idContact,
          },
        },
      });
      if (data.enviarMensaje === null || !data.enviarMensaje.estado) {
        alert('Ocurrio un error al mandar el mensaje');
      }
      setMessageSend('');
    }
  };

  function changeDataMessage(data) {
    let dataMessages = [];
    for (const key in data.getMessages) {
      let datos = {};
      const id = data.getMessages[key].id;
      const text = data.getMessages[key].message;
      const time = data.getMessages[key].time;
      const from = data.getMessages[key].from.id;
      datos = { ...datos, id, text, time, from };
      dataMessages.push(datos);
    }
    messages = dataMessages;
  }
 */

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop: 55,
    // backgroundColor: "#f00",
  },
});
