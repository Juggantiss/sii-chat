import React, { useState, useEffect } from 'react';
import * as Contact from 'expo-contacts';

import { gql, useMutation } from '@apollo/client';

import {
  TouchableOpacity,
  View,
  FlatList,
  StyleSheet,
  Text,
} from 'react-native';
import Loading from '../components/Loading';

const UPDATE_CONTACTS_MUTATION = gql`
  mutation updateContacts($input: UpdateContactsInput!) {
    updateContacts(input: $input) {
      id
      alias
      status
      phone
      user {
        id
      }
      lastMessage
    }
  }
`;

const ejemplos = [
  {
    id: 1,
    name: 'Alexis ',
    estado: 'frase',
    bgColor: '#B8005F',
    uri: 'https://picsum.photos/200/200',
  },
  {
    id: 2,
    name: 'Natasha',
    estado: 'frase',
    bgColor: '#8103BB',
    uri: 'https://picsum.photos/200/200',
  },
  {
    id: 3,
    name: 'Kelly N',
    estado: 'frase',
    bgColor: '#009709',
    uri: 'https://picsum.photos/200/200',
  },
  {
    id: 4,
    name: 'Brandon',
    estado: 'frase',
    bgColor: '#03004C',
    uri: 'https://picsum.photos/200/200',
  },
  {
    id: 5,
    name: 'Grace S',
    estado: 'frase',
    bgColor: '#FF4937',
    uri: 'https://picsum.photos/200/200',
  },
  {
    id: 6,
    name: 'Lisa',
    estado: 'frase',
    bgColor: '#8103BB',
    uri: 'https://picsum.photos/200/200',
  },
  {
    id: 7,
    name: 'John D ',
    estado: 'frase',
    bgColor: '#009709',
    uri: 'https://picsum.photos/200/200',
  },
  {
    id: 8,
    name: 'Juan',
    estado: 'frase',
    bgColor: '#03004C',
    uri: 'https://picsum.photos/200/200',
  },
  {
    id: 9,
    name: 'Miguel',
    estado: 'frase',
    bgColor: '#FF4937',
    uri: 'https://picsum.photos/200/200',
  },
  {
    id: 10,
    name: 'Rafa',
    estado: 'frase',
    bgColor: '#B8005F',
    uri: 'https://picsum.photos/200/200',
  },
];

let navi = null;
let idUser = null;

const renderItem = ({ item }) => {
  return (
    <TouchableOpacity
      style={styles.contUser}
      onPress={() =>
        navi.navigate('Chat', {
          id: item.user.id,
          name: item.alias,
          uri: 'https://picsum.photos/200/200',
        })
      }
    >
      <View style={[styles.iconUser, { backgroundColor: '#03004C' }]}>
        <Text style={styles.iconLetter}>{item.alias.charAt(0)}</Text>
      </View>
      <View style={styles.nameUser}>
        <Text style={styles.name}>{item.alias}</Text>
        {/* <Text style={styles.estado}>{item.estado}</Text> */}
      </View>
    </TouchableOpacity>
  );
};

export default function Contacts({ route, navigation }) {
  const [loading, setLoading] = useState(false);
  const [contacts, setContacts] = useState([]);
  navi = navigation;

  const setContactsToState = (contacts) => {
    setContacts(contacts);
  };

  useEffect(() => {
    const { contacts } = route.params;
    setContactsToState(contacts);
  }, []);

  const [updateContacts] = useMutation(UPDATE_CONTACTS_MUTATION);

  const handleUpdateContacts = async () => {
    try {
      setLoading(true);
      const { status } = await Contact.requestPermissionsAsync();
      if (status === 'granted') {
        const { data } = await Contact.getContactsAsync({
          fields: [Contact.Fields.PhoneNumbers],
        });
        if (data.length > 0) {
          let numbers = [];
          for (const contact of data) {
            let number = '';
            const name = contact.name;
            const phones = contact.phoneNumbers;
            for (const key in phones) {
              number = phones[key].number.replace(/\s+/g, '');
            }
            numbers.push({ alias: name, phone: number });
          }

          const request = await updateContacts({
            variables: {
              input: {
                contacts: numbers,
              },
            },
          });

          if (request.data) {
            setContacts(request.data.updateContacts);
          }
        }
      }
    } catch (error) {
      console.log('error');
    } finally {
      setLoading(false);
    }
  };

  if (loading) return <Loading />;

  return (
    <View style={styles.content}>
      <View style={styles.contList}>
        <FlatList
          data={contacts}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ paddingBottom: 70 }}
          // contentInset={{ right: 20, top: 0, left: 0, bottom: 0 }}
        />
        <TouchableOpacity
          style={{ marginBottom: 20 }}
          onPress={handleUpdateContacts}
        >
          <Text>Actualizar</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  content: {
    width: '100%',
    height: '100%',
    backgroundColor: '#03004C',
  },
  contList: {
    width: '100%',
    height: '100%',
    backgroundColor: '#fff',
    borderTopRightRadius: 35,
    borderTopLeftRadius: 35,
    paddingHorizontal: 30,
    // paddingBottom: 50,
  },
  contUser: {
    width: '100%',
    height: 75,
    // backgroundColor: "#ff0",
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  iconUser: {
    // backgroundColor: "#ff0",
    width: 50,
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconLetter: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#fff',
  },
  nameUser: {
    width: '80%',
    height: 50,
    // backgroundColor: "#ff0",
    // padding: 5,
    justifyContent: 'center',
  },
  name: {
    fontSize: 17,
  },
  estado: {
    fontSize: 13,
    color: '#878787',
  },
});
