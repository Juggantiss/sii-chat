import React, { useEffect, useState } from 'react';
import { gql, useQuery } from '@apollo/client';

// import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
// import { AntDesign, MaterialIcons } from "@expo/vector-icons";

// import Segundo from "./ActionBarImage";
import { AntDesign } from '@expo/vector-icons';
import ListContact from './List';

import { StyleSheet, Image, View, TouchableOpacity } from 'react-native';
import Loading from '../components/Loading';

// const Tab = createMaterialBottomTabNavigator();

const CONTACTS_QUERY = gql`
  query {
    me {
      contacts {
        id
        alias
        status
        phone
        lastMessage
        user {
          id
        }
      }
    }
  }
`;

export default function Home({ navigation }) {
  const [contacts, setContacts] = useState([]);
  const { loading, error, data, refetch } = useQuery(CONTACTS_QUERY, {
    fetchPolicy: 'no-cache',
  });

  useEffect(() => {
    if (!loading && data) {
      setContacts(data.me.contacts);
    }
  }, [data]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', async () => {
      if (refetch) {
        try {
          const { data } = await refetch();
          setContacts(data.me.contacts);
        } catch (error) {}
      }
    });

    return unsubscribe;
  }, [navigation]);

  if (loading) return <Loading />;

  if (error) return null;

  const sendContact = (contacts) => () => {
    navigation.navigate('Contacts', { contacts });
  };

  return (
    <>
      <View style={styles.container}>
        <ListContact
          navigation={navigation}
          contacts={contacts}
          refetch={refetch}
        />
        <TouchableOpacity
          style={styles.btnNewChat}
          onPress={sendContact(contacts)}
        >
          <AntDesign name='plus' size={24} color='#fff' />
        </TouchableOpacity>
      </View>
      {/* <Tab.Navigator
        initialRouteName="Message"
        labeled={false}
        // activeColor="#e91e63"
        barStyle={{ backgroundColor: "#03004C" }}
      >
        <Tab.Screen
          name="Message"
          component={ListContact}
          options={{
            tabBarLabel: "Chat",
            tabBarIcon: () => (
              <AntDesign name="message1" size={24} color="#fff" />
            ),
          }}
        />
        <Tab.Screen
          name="History"
          component={Segundo}
          options={{
            tabBarLabel: "Historias",
            tabBarIcon: () => (
              <MaterialIcons name="motion-photos-on" size={24} color="#fff" />
            ),
          }}
        />
        <Tab.Screen
          name="contacts"
          component={Segundo}
          options={{
            tabBarLabel: "Contactos",
            tabBarIcon: () => <AntDesign name="user" size={24} color="#fff" />,
          }}
        />
        <Tab.Screen
          name="profile"
          component={Segundo}
          options={{
            tabBarLabel: "Perfil",
            tabBarIcon: () => (
              <Image
                style={{ width: 25, height: 25, borderRadius: 15 }}
                source={{
                  uri: "https://picsum.photos/200/200",
                }}
              />
            ),
          }}
        />
      </Tab.Navigator> */}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#03004C',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  btnNewChat: {
    position: 'absolute',
    bottom: 20,
    right: 20,
    backgroundColor: '#03004C',
    width: 50,
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
