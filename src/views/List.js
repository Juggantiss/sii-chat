import React, { useState } from 'react';
// import { StatusBar } from "expo-status-bar";
import { Ionicons } from '@expo/vector-icons';
import { gql, useQuery } from '@apollo/client';
import ItiCrypt from '../utils/ItiCrypt';

import {
  TouchableOpacity,
  View,
  FlatList,
  StyleSheet,
  Text,
  Image,
} from 'react-native';
import Loading from '../components/Loading';

const GETMESSAGES_QUERY = gql`
  query GetMessage($id: ID) {
    misDatos(id: $id) {
      messages {
        id
        message
        from {
          id
          name
        }
        to {
          id
          name
        }
      }
    }
  }
`;

const ejemplos = [
  {
    id: 1,
    name: 'Alexis ',
    newMessage: false,
    message: 'Okay.',
    timeMsg: '8:00',
    lastMessage: false,
    deliveredMsg: false,
    bgColor: '#B8005F',
    uri: 'https://picsum.photos/200/200',
  },
  {
    id: 2,
    name: 'Natasha',
    newMessage: true,
    message: 'Ok. Thanks',
    timeMsg: 'Ayer',
    lastMessage: true,
    deliveredMsg: true,
    bgColor: '#8103BB',
    uri: 'https://picsum.photos/200/200',
  },
  {
    id: 3,
    name: 'Kelly N',
    newMessage: true,
    message: 'Thanks',
    timeMsg: 'Ayer',
    lastMessage: true,
    deliveredMsg: false,
    bgColor: '#009709',
    uri: 'https://picsum.photos/200/200',
  },
  {
    id: 4,
    name: 'Brandon',
    newMessage: true,
    message: 'Im already up',
    timeMsg: 'Ayer',
    lastMessage: false,
    deliveredMsg: false,
    bgColor: '#03004C',
    uri: 'https://picsum.photos/200/200',
  },
  {
    id: 5,
    name: 'Grace S',
    newMessage: false,
    message: 'Are you ready to go',
    timeMsg: 'Ayer',
    lastMessage: false,
    deliveredMsg: false,
    bgColor: '#FF4937',
    uri: 'https://picsum.photos/200/200',
  },
  {
    id: 6,
    name: 'Lisa',
    newMessage: true,
    message: 'Im home',
    timeMsg: '29/04/21',
    lastMessage: false,
    deliveredMsg: false,
    bgColor: '#8103BB',
    uri: 'https://picsum.photos/200/200',
  },
  {
    id: 7,
    name: 'John D ',
    newMessage: true,
    message: 'Hey',
    timeMsg: '29/04/21',
    lastMessage: false,
    deliveredMsg: false,
    bgColor: '#009709',
    uri: 'https://picsum.photos/200/200',
  },
  {
    id: 8,
    name: 'Juan',
    newMessage: true,
    message: 'Hey',
    timeMsg: '29/04/21',
    lastMessage: false,
    deliveredMsg: false,
    bgColor: '#03004C',
    uri: 'https://picsum.photos/200/200',
  },
  {
    id: 9,
    name: 'Miguel',
    newMessage: true,
    message: 'Hey',
    timeMsg: '29/04/21',
    lastMessage: false,
    deliveredMsg: false,
    bgColor: '#FF4937',
    uri: 'https://picsum.photos/200/200',
  },
  {
    id: 10,
    name: 'Rafa',
    newMessage: true,
    message: 'Hey',
    timeMsg: '29/04/21',
    lastMessage: false,
    deliveredMsg: false,
    bgColor: '#B8005F',
    uri: 'https://picsum.photos/200/200',
  },
];

export default function List({ navigation, contacts, refetch }) {
  const [chats, setChats] = useState([]);
  const itiCrypt = new ItiCrypt();
  /*const { loading, error, data } = useQuery(GETMESSAGES_QUERY, {
    variables: { id },
  });*/

  //if (loading) return <Loading />;

  //if (error) alert("Ha ocurrido un error al traer tus chats");
  // const sendContacts = () => {
  //   navigation.navigate("Contacts");
  // };

  const ContMsg = () => {
    return (
      <View style={styles.contStatus}>
        <Text style={styles.msgStatus}>1</Text>
      </View>
    );
  };

  const updateForNewMessages = async () => {
    try {
      const { data } = await refetch();
      if (data.me) {
        console.log(data.me.contacts);
      }
    } catch (error) {
      console.log(error);
      console.log('Error en List.js');
    }
  };

  const StatusMsg = ({ lastMessage, deliveredMsg }) => {
    if (lastMessage) {
      if (deliveredMsg) {
        return <Ionicons name='checkmark-done' size={18} color='#03004C' />;
      } else {
        return <Ionicons name='checkmark' size={18} color='#03004C' />;
      }
    } else {
      return null;
    }
  };

  const renderItem = ({ item }) => {
    if (item.lastMessage) {
      return (
        <TouchableOpacity
          style={styles.contUser}
          onPress={() =>
            navigation.navigate('Chat', {
              id: item.user.id,
              name: item.alias,
              uri: 'https://picsum.photos/200/200',
            })
          }
        >
          {/* <View style={[styles.iconUser, { backgroundColor: item.bgColor }]}>
        <Text style={styles.iconLetter}>{item.name.charAt(0)}</Text>
      </View> */}
          <View style={styles.iconUser}>
            <Image
              style={styles.imgUser}
              source={{ uri: 'https://picsum.photos/200/200' }}
            />
          </View>
          <View style={styles.msgUser}>
            <Text style={styles.name}>{item.alias}</Text>
            <Text
              style={[
                styles.textMsg,
                item.newMessage ? styles.view : styles.noView,
              ]}
            >
              {itiCrypt.readMessage(item.lastMessage)}
            </Text>
          </View>
          <View style={styles.dataUser}>
            <Text style={styles.msgTime}>{item.timeMsg}</Text>
            {item.newMessage ? (
              // ? <Ionicons name="checkmark-done" size={18} color="#03004C" />
              <StatusMsg
                lastMessage={item.lastMessage}
                deliveredMsg={item.deliveredMsg}
              />
            ) : (
              <ContMsg />
            )}
          </View>
        </TouchableOpacity>
      );
    }
    return null;
  };

  return (
    <View style={styles.content}>
      <View style={styles.contList}>
        <FlatList
          data={contacts}
          renderItem={renderItem}
          keyExtractor={(item) => item.id.toString()}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ paddingBottom: 70 }}
          // contentInset={{ right: 20, top: 0, left: 0, bottom: 0 }}
          onRefresh={updateForNewMessages}
          refreshing={false}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  content: {
    width: '100%',
    height: '100%',
    backgroundColor: '#03004C',
  },
  contList: {
    width: '100%',
    height: '100%',
    backgroundColor: '#fff',
    borderTopRightRadius: 35,
    borderTopLeftRadius: 35,
    paddingHorizontal: 30,
    // paddingBottom: 50,
  },
  contUser: {
    width: '100%',
    height: 75,
    // backgroundColor: "#ff0",
    // borderBottomWidth: 1,
    // borderBottomColor: "#000",
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  iconUser: {
    backgroundColor: '#f00',
    width: 50,
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgUser: {
    width: 50,
    height: 50,
    borderRadius: 25,
  },
  msgUser: {
    width: '64%',
    height: 50,
    // backgroundColor: "#ff0",
    padding: 10,
    justifyContent: 'center',
  },
  name: {
    fontSize: 17,
  },
  textMsg: {
    fontSize: 13,
    color: '#878787',
  },
  view: {
    color: '#878787',
  },
  noView: {
    color: '#03004C',
  },
  dataUser: {
    width: '18%',
    height: 50,
    // backgroundColor: "#ff0",
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  msgTime: {
    fontSize: 12,
    color: '#878787',
    marginBottom: 5,
  },
  contStatus: {
    backgroundColor: '#03004C',
    width: 18,
    height: 18,
    borderRadius: 9,
    justifyContent: 'center',
    alignItems: 'center',
  },
  msgStatus: {
    fontSize: 9,
    color: '#fff',
  },
});
