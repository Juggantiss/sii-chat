import React, { useState, useEffect } from 'react';
import * as Contact from 'expo-contacts';
import { setToken } from '../../token';

// Utils
import ErrorManager from '../utils/ErrorManager';

import { gql, useMutation } from '@apollo/client';

import { View, TouchableOpacity, StyleSheet, Text } from 'react-native';
import ExtendForm from '../components/User/ExtendForm';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import Loading from '../components/Loading';

const NEW_SESSION_MUTATION = gql`
  mutation user($user: String!, $password: String!) {
    newSession(user: $user, password: $password)
  }
`;

export default function NewUser({ route, navigation }) {
  const [variables, setVariables] = useState({
    user: route.params.phone,
    password: '',
  });
  const [loading, setLoading] = useState(false);

  const [newSession] = useMutation(NEW_SESSION_MUTATION);

  useEffect(() => {
    (async () => {
      const { status } = await Contact.requestPermissionsAsync();
      if (status === 'granted') {
        const { data } = await Contact.getContactsAsync({
          fields: [Contact.Fields.PhoneNumbers],
        });

        if (data.length > 0) {
          getNumbersContact(data);
        }
      }
    })();
  }, []);

  if (loading) return <Loading />;

  const getNumbersContact = (data) => {
    let contacts = [];
    for (const contact of data) {
      let number = '';
      const name = contact.name;
      const phones = contact.phoneNumbers;
      for (const key in phones) {
        number = phones[key].number.replace(/\s+/g, '');
      }
      contacts.push({ alias: name, phone: number });
    }
    setVariables({ ...variables, contacts });
  };

  const handleChange = (field) => (text) => {
    setVariables({ ...variables, [field]: text });
  };

  const onPress = (variables) => async () => {
    try {
      setLoading(true);
      const { data } = await newSession({ variables });
      setLoading(false);
      if (data?.newSession) {
        await setToken(data.newSession.token);
        return navigation.navigate('Home');
      }
      throw { message: 'No podemos hacer nada por ti, por ahora' };
    } catch (error) {
      const errorManager = new ErrorManager(error);
      errorManager.showMessage('Datos incorrectos');
    }
  };

  return (
    <>
      <KeyboardAwareScrollView contentContainerStyle={styles.container}>
        <View style={styles.content}>
          <View style={styles.contImg}>
            <View style={styles.image}></View>
          </View>
        </View>
        <View style={styles.contForm}>
          <ExtendForm
            iconName='lock'
            fieldName='Contraseña'
            paragraph='Necesitamos tu contraseña para verificar que eres tú.'
            value={variables.password}
            secureTextEntry={true}
            style={styles.input}
            onChangeText={handleChange('password')}
            placeholder='Escribir aquí'
            placeholderTextColor='#fff'
            color='#fff'
          />
          <TouchableOpacity
            style={[styles.btnPrimary, styles.btn]}
            onPress={onPress(variables)}
          >
            <Text style={styles.textBtn}>Ingresar</Text>
          </TouchableOpacity>
        </View>
      </KeyboardAwareScrollView>
    </>
  );
}
// const { height } = (Dimensions.get('window')*.2) + 25;
const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: '#03004C',
  },
  content: {
    // backgroundColor: "#f00",
    width: '100%',
    height: '35%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  contImg: {
    // backgroundColor: "#f00",
    width: 150,
    height: 150,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
  image: {
    backgroundColor: '#ffffff80',
    width: 150,
    height: 150,
    borderRadius: 75,
  },
  btnImg: {
    position: 'absolute',
    backgroundColor: '#00D9F6',
    justifyContent: 'center',
    alignItems: 'center',
    width: 50,
    height: 50,
    borderRadius: 25,
    bottom: 0,
    right: 0,
  },
  iconCamera: {
    color: '#fff',
    fontSize: 20,
  },
  contForm: {
    // backgroundColor: "#00f",
    width: '100%',
    height: '65%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 100,
  },
  input: {
    // backgroundColor: "#000",
    width: '100%',
    height: 40,
    fontSize: 15,
  },
  btn: {
    width: '50%',
    height: 45,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '10%',
  },
  btnPrimary: {
    backgroundColor: '#00D9F6',
  },
});
