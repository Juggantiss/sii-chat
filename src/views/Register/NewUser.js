import React, { useState, useEffect } from 'react';
import * as Contact from 'expo-contacts';
import { setToken } from '../../../token';

// Utils
import ErrorManager from '../../utils/ErrorManager';

import { gql, useMutation } from '@apollo/client';

import { View, TouchableOpacity, StyleSheet, Text } from 'react-native';
import ExtendForm from '../../components/User/ExtendForm';

import { FontAwesome5 } from '@expo/vector-icons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import Loading from '../../components/Loading';

const CREATE_USER_MUTATION = gql`
  mutation user($input: NewUserInput!) {
    createUser(input: $input) {
      token
    }
  }
`;

export default function NewUser({ route, navigation }) {
  const [variables, setVariables] = useState({
    name: '',
    email: '',
    password: '',
    phone: route.params.phone,
    contacts: [],
  });
  const [loading, setLoading] = useState(false);

  const [createUser] = useMutation(CREATE_USER_MUTATION);

  useEffect(() => {
    (async () => {
      const { status } = await Contact.requestPermissionsAsync();
      if (status === 'granted') {
        const { data } = await Contact.getContactsAsync({
          fields: [Contact.Fields.PhoneNumbers],
        });

        if (data.length > 0) {
          getNumbersContact(data);
        }
      }
    })();
  }, []);

  if (loading) return <Loading />;

  const getNumbersContact = (data) => {
    let contacts = [];
    for (const contact of data) {
      let number = '';
      const name = contact.name;
      const phones = contact.phoneNumbers;
      for (const key in phones) {
        number = phones[key].number.replace(/\s+/g, '');
      }
      contacts.push({ alias: name, phone: number });
    }
    setVariables({ ...variables, contacts });
  };

  const handleChange = (field) => (text) => {
    setVariables({ ...variables, [field]: text });
  };

  const onPress = async () => {
    try {
      setLoading(true);
      const input = variables;
      const { data } = await createUser({ variables: { input } });
      setLoading(false);
      if (data?.createUser) {
        await setToken(data.createUser.token);
        return navigation.navigate('Home');
      }
      throw { message: 'Esto no debió suceder, una disculpa' };
    } catch (error) {
      const errorManager = new ErrorManager(error);
      errorManager.showMessage();
    } finally {
      setLoading(false);
    }
  };

  return (
    <>
      <KeyboardAwareScrollView contentContainerStyle={styles.container}>
        <View style={styles.content}>
          <View style={styles.contImg}>
            <View style={styles.image}></View>
            <TouchableOpacity style={styles.btnImg}>
              <FontAwesome5 style={styles.iconCamera} name='camera' />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.contForm}>
          <ExtendForm
            iconName='user-alt'
            fieldName='Nombre'
            // paragraph='Este nombre será visible para tus contactos de ChatApp.'
            value={variables.name}
            style={styles.input}
            onChangeText={handleChange('name')}
            placeholder='Escribir aquí'
            placeholderTextColor='#fff'
            color='#fff'
          />
          <ExtendForm
            iconName='envelope-open-text'
            fieldName='Correo electronico'
            // paragraph='Necesitamos tu correo para cuidar mejor tu información.'
            value={variables.email}
            keyboardType='email-address'
            style={styles.input}
            onChangeText={handleChange('email')}
            placeholder='Escribir aquí'
            placeholderTextColor='#fff'
            color='#fff'
          />
          <ExtendForm
            iconName='lock'
            fieldName='Contraseña'
            // paragraph='Necesitamos una contraseña para cuidar mejor tu información.'
            value={variables.password}
            secureTextEntry={true}
            style={styles.input}
            onChangeText={handleChange('password')}
            placeholder='Escribir aquí'
            placeholderTextColor='#fff'
            color='#fff'
          />
          <TouchableOpacity
            style={[styles.btnPrimary, styles.btn]}
            onPress={onPress}
          >
            <Text style={styles.textBtn}>Guardar</Text>
          </TouchableOpacity>
        </View>
      </KeyboardAwareScrollView>
    </>
  );
}
// const { height } = (Dimensions.get('window')*.2) + 25;
const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: '#03004C',
  },
  content: {
    // backgroundColor: "#f00",
    width: '100%',
    height: '35%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  contImg: {
    // backgroundColor: "#f00",
    width: 150,
    height: 150,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
  image: {
    backgroundColor: '#ffffff80',
    width: 150,
    height: 150,
    borderRadius: 75,
  },
  btnImg: {
    position: 'absolute',
    backgroundColor: '#00D9F6',
    justifyContent: 'center',
    alignItems: 'center',
    width: 50,
    height: 50,
    borderRadius: 25,
    bottom: 0,
    right: 0,
  },
  iconCamera: {
    color: '#fff',
    fontSize: 20,
  },
	contForm: {
    // backgroundColor: "#fff",
    width: '100%',
    height: '65%',
    justifyContent: 'center',
    alignItems: 'center',
		paddingVertical: 50,
  },
  btn: {
    width: '50%',
    height: 45,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '5%',
  },
  btnPrimary: {
    backgroundColor: '#00D9F6',
  },
});