import React, { useState } from 'react';
import { View, StyleSheet, Text } from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import Form from '../../components/User/Form';

import { gql, useMutation } from '@apollo/client';

const HAS_ACCOUNT_MUTATION = gql`
  mutation ($phone: String!) {
    hasAccount(phone: $phone)
  }
`;

export default function User({ navigation }) {
  const [phone, setPhone] = useState('');
  const [hasAccount] = useMutation(HAS_ACCOUNT_MUTATION);

  const handleChange = (text) => {
    const value = text.replace(/ /g, '');
    if (value.match(/[0-9]+$/)) {
      setPhone(value);
    } else {
      if (value === '') {
        setPhone(value);
      }
    }
  };

  const onPress = async () => {
    try {
      if (phone.length === 10) {
        const { data } = await hasAccount({
          variables: {
            phone,
          },
        });
        if (!data.hasAccount) {
          return navigation.navigate('NewUser', { phone });
        } else {
          return navigation.navigate('Login', { phone });
        }
      } else {
        alert('La longitud debe ser de 10 digitos');
      }
    } catch (error) {
      console.log(error);
      alert('Estamos trabajando en esto, una disculpa');
    }
  };

  return (
    <>
      <KeyboardAwareScrollView
        contentContainerStyle={{ flexGrow: 1, backgroundColor: '#fff' }}
      >
        <View style={styles.contTop}>
          <Text style={styles.nameApp}>ITI CHAT</Text>
          <Text style={styles.descApp}>Tecnm Campus Istmo</Text>
        </View>
        <View style={styles.contForm}>
          <Form
            value={phone}
            onChangeText={(text) => handleChange(text)}
            onPressNext={onPress}
          />
        </View>
      </KeyboardAwareScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  contTop: {
    width: '100%',
    height: '30%',
    backgroundColor: '#03004C',
    borderBottomRightRadius: 40,
    borderBottomLeftRadius: 40,
    justifyContent: 'center',
    padding: 40,
  },
  nameApp: {
    color: '#00D9F6',
    fontSize: 40,
    fontWeight: 'bold',
  },
  descApp: {
    color: '#fff',
    fontSize: 15,
  },
  contForm: {
    width: '100%',
    height: '70%',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 5,
    paddingBottom: 5,
  },
});
