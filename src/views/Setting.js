import React from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';

export default function Setting() {
  const navigation = useNavigation();

  const Item = ({ nav, icon, txtName, txtDesc }) => (
    <>
      <TouchableOpacity
        style={styles.item}
        onPress={() => navigation.navigate(nav)}
      >
        <View style={styles.itemIcon}>
          <Ionicons name={icon} size={25} color='#03004C' />
        </View>
        <View style={styles.itemInfo}>
          <Text style={styles.itemName}>{txtName}</Text>
          <Text style={styles.itemDesc}>{txtDesc}</Text>
        </View>
      </TouchableOpacity>
    </>
  );

  return (
    <>
      <ScrollView style={styles.scrollView}>
        <TouchableOpacity style={styles.contPerfil}>
          <View style={styles.perfilImg}>
            <Image
              style={styles.imgUser}
              source={{ uri: 'https://picsum.photos/200/200' }}
            />
          </View>
          <View style={styles.perfilName}>
            <Text style={styles.name}>Alexis Regalado</Text>
            <Text style={styles.desc}>Lorem Ipsum</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.contSettings}>
          <Item
            nav='Cuenta'
            icon='key'
            txtName='Cuenta'
            txtDesc='privacidad y seguridad'
          />
          <Item
            nav='Cuenta'
            icon='help-circle'
            txtName='Ayuda'
            txtDesc='Centro de ayuda, contactanos, política de privacidad'
          />
        </View>
        <View style={styles.contInfo}>
          <TouchableOpacity style={styles.item}>
            <View style={styles.itemIcon}>
              <Ionicons name='person' size={25} color='#03004C' />
            </View>
            <View style={styles.itemInfo}>
              <Text style={styles.itemName}>Invitar amigos</Text>
            </View>
          </TouchableOpacity>
          <Text style={[styles.itemDesc, { marginTop: 20 }]}>from</Text>
          <Text style={styles.itemName}>CINERE</Text>
        </View>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contPerfil: {
    // backgroundColor: '#ff0',
    width: '100%',
    height: 100,
    paddingHorizontal: 20,
    flexDirection: 'row',
    borderBottomColor: '#F3F4F4',
    borderBottomWidth: 1,
  },
  perfilImg: {
    // backgroundColor: '#f00',
    width: '25%',
    height: '100%',
    justifyContent: 'center',
  },
  imgUser: {
    width: 70,
    height: 70,
    borderRadius: 35,
  },
  perfilName: {
    // backgroundColor: '#f0f',
    width: '75%',
    height: '100%',
    justifyContent: 'center',
    paddingHorizontal: 5,
  },
  name: {
    color: '#03004C',
    fontSize: 20,
  },
  desc: {
    color: '#959595',
    fontSize: 15,
  },
  contSettings: {
    // backgroundColor: '#ff0',
    padding: 20,
    borderBottomColor: '#F3F4F4',
    borderBottomWidth: 1,
  },
  item: {
    width: '100%',
    height: 80,
    // backgroundColor: '#ff0',
    flexDirection: 'row',
  },
  itemIcon: {
    // backgroundColor: '#00f',
    justifyContent: 'center',
    alignItems: 'center',
    width: '18%',
    height: '100%',
  },
  itemInfo: {
    // backgroundColor: '#f00',
    justifyContent: 'center',
    width: '82%',
    height: '100%',
  },
  itemName: {
    color: '#03004C',
    fontSize: 15,
  },
  itemDesc: {
    color: '#959595',
    fontSize: 12,
  },
  contInfo: {
    // backgroundColor: '#f00',
    width: '100%',
    height: 200,
    paddingHorizontal: 20,
    // justifyContent: 'center',
    alignItems: 'center',
  },
});
