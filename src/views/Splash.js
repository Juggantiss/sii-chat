import React, { useEffect, useState } from 'react';
import { ActivityIndicator, StyleSheet, View, Image, Text } from 'react-native';
import { deleteToken } from '../../token';
import { gql, useMutation } from '@apollo/client';

const VERIFY_SESSION_MUTATION = gql`
  mutation {
    verifyToken
  }
`;

export default function Splash({ navigation }) {
  const [loading, setLoading] = useState(true);
  const [verifyToken] = useMutation(VERIFY_SESSION_MUTATION, {
    fetchPolicy: 'no-cache',
  });

  const validateSession = async () => {
    try {
      const { data } = await verifyToken();
      if (data?.verifyToken) {
        navigation.navigate('Home');
      } else {
        await deleteToken();
        setLoading(false);
        navigation.navigate('User');
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    validateSession();
  }, []);

  if (loading) {
    return (
      <View style={styles.container}>
        <Image
          style={styles.imgLogo}
          source={require('../../assets/logo.png')}
        />
        <Text style={styles.nameApp}>ITI Chat</Text>
        <Text style={styles.descApp}>Un lugar seguro para comunicarte</Text>
        <ActivityIndicator size='large' color='#FFF' />
      </View>
    );
  }

  return null;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#03004C',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgLogo: {
    width: 100,
    height: 100,
    resizeMode: 'stretch',
    marginBottom: 20,
  },
  nameApp: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  descApp: {
    color: '#fff',
    fontSize: 12,
    marginBottom: 30,
  },
});
